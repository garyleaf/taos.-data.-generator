﻿using Taos.Data.Generator;

LogUtil.OnError += onError;

void onError(string obj)
{
    Console.WriteLine(DateTime.Now +" ERROR " +  obj);
}

LogUtil.OnInfo += onInfo;

void onInfo(string obj)
{
    Console.WriteLine(DateTime.Now + " INFO " + obj);
}

LogUtil.OnSucc += onInfo;
LogUtil.OnTrace += onInfo;
LogUtil.OnWarn += onInfo;

Manager.start();


Console.ReadKey();




