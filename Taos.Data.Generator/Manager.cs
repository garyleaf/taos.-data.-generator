﻿using Newtonsoft.Json;
using System.Text;
using System.Timers;
namespace Taos.Data.Generator
{
    public interface IString
    {
        public string ToString();
    }


    public static class RandomManager
    {
        public static Random random = new Random();

        public static int nextInt(int min, int max)
        {
            return Convert.ToInt32(Math.Floor(random.Next(min * 100, max * 100) / 100.0 + 0.5));
        }
    }
    public class RandomDouble: IString
    {
        double min = 0;
        double max = 1;

        double dist = 1;
        public RandomDouble(double min, double max)
        {
            this.min = min;
            this.max = max;
            dist = max - min;

        }
        public double value
        {
            get
            {
                return  min + RandomManager.random.NextDouble() * dist;
            }
        }

        public override string ToString()
        {
            return value.ToString("0.00000");
        }
    }


    public class IncrementalRandomDouble : IString
    {
        double min = 0;
        double max = 1;

        double dist = 1;

        double ori = 0;
        public IncrementalRandomDouble(double min, double max)
        {
            this.min = min;
            this.max = max;
            dist = max - min;
        }

        double increment = 0;
        public double value
        {
            get
            {
                increment = min + RandomManager.random.NextDouble() * dist;
                ori += increment;
                return ori;
            }
        }

        public override string ToString()
        {
            return value.ToString("0.00000");
        }
    }

    public class IncrementalRandomInt : IString
    {
        int min = 0;
        int max = 1;


        int ori = 0;
        public IncrementalRandomInt(int min, int max)
        {
            this.min = min;
            this.max = max;
            if (min > max)
            {
                throw new Exception("invaild min and max");
            }
        }

        int increment = 0;
        public int value
        {
            get
            {
                increment = min + RandomManager.random.Next(min, max);
                ori += increment;
                return ori;
            }
        }

        public override string ToString()
        {
            return value.ToString();
        }
    }


    public class RandomFloat : IString
    {
        float min = 0;
        float max = 1;

        float dist = 1;
        public RandomFloat(float min, float max)
        {
            this.min = min;
            this.max = max;
            dist = max - min;

        }
        public float value
        {
            get
            {
                return min + RandomManager.random.NextSingle() * dist;
            }
        }

        public override string ToString()
        {
            return value.ToString("0.00000");
        }
    }

    public class RandomInt : IString
    {
        int min = 0;
        int max = 1;
        public RandomInt(int min, int max)
        {
            if(min > max)
            {
                var _ = min;
                min = max;
                max = _;
            }

            this.min = min;
            this.max = max;
        }
        public int value
        {
            get
            {
                return  RandomManager.nextInt(min,max);
            }
        }
        public override string ToString()
        {
            return value.ToString();
        }
    }


    public class RandomBool : IString
    {
        public RandomBool()
        {

        }
        public bool value
        {
            get
            {
                return RandomManager.random.Next(0,10) > 3? true : false;
            }
        }

        public override string ToString()
        {
            return value.ToString();
        }
    }

    public class RawString:IString
    {
        string s;
        public RawString (string s)
        {
            this.s = s;
        }

        public override string ToString()
        {
            return s;
        }

    }


    public class TaosTask
    {
        public TaosTaskConfig cfg;
        public TaosTask(TaosTaskConfig cfg)
        {
            this.cfg = cfg;
            parse();
        }

        public List<IString> parts = new List<IString>();

        public IString parseVStr(string s)
        {
            //RB, RD, RF, RI
            //try
            //{
                var txt = s.Trim().ToUpper();
                if(s.StartsWith("RB"))
                {
                    return new RandomBool();
                }
                else if(s.StartsWith("RI"))
                {
                    int min = 0;
                    int max = 0;
                    var ss = s.Substring(2);
                    ss = ss.TrimStart('(');
                    ss = ss.TrimEnd(')');
                    var lst = ss.Split(',');
                    min = Convert.ToInt32(lst[0]);
                    max = Convert.ToInt32(lst[1]);

                    return new RandomInt(min, max);
                }
                else if (s.StartsWith("RF"))
                {
                    float min = 0;
                    float max = 0;
                    var ss = s.Substring(2);
                    ss = ss.TrimStart('(');
                    ss = ss.TrimEnd(')');
                    var lst = ss.Split(',');
                    min = Convert.ToSingle(lst[0]);
                    max = Convert.ToSingle(lst[1]);

                    return new RandomFloat(min, max);
                }
                else if (s.StartsWith("RD"))
                {
                    double min = 0;
                    double max = 0;
                    var ss = s.Substring(2);
                    ss = ss.TrimStart('(');
                    ss = ss.TrimEnd(')');
                    var lst = ss.Split(',');
                    min = Convert.ToDouble(lst[0]);
                    max = Convert.ToDouble(lst[1]);

                    return new RandomDouble(min, max);
                }

                throw new Exception($"unknow token type, {cfg.sql}");
            //}
            //catch (Exception e)
            //{
            //    throw new Exception($"invalid token found, {cfg.sql}, exception: {e}");
            //}
        }

        public void parse()
        {
            bool start = false;

            var str = "";
            var vstr = "";
            foreach(var c in cfg.sql)
            {
                if(c == '{')
                {
                    start = true;
                    if(str.Length > 0)
                    {
                        parts.Add(new RawString(str));
                    }
                    str = "";
                    continue;
                }

                if(c == '}')
                {
                    start = false;
                    IString istring = parseVStr(vstr);
                    parts.Add(istring);
                    vstr = "";
                    continue;
                }

                if(start)
                {
                    vstr += c;
                }
                else 
                {
                    str += c;
                }
            }

            if (str.Length > 0)
            {
                parts.Add(new RawString(str));
            }
        }


        System.Timers.Timer _timer;
        public System.Timers.Timer timer
        {
            get
            {
                if (_timer == null)
                {
                    _timer = new System.Timers.Timer(cfg.interval);
                    _timer.Elapsed += OnTimerElapsed;
                }
                return _timer;
            }
        }

        private void OnTimerElapsed(object? sender, ElapsedEventArgs e)
        {
            var sb = new StringBuilder();

            foreach (var p in parts)
            {
                sb.Append(p.ToString());
            }

            var sql = sb.ToString();

            TaosUtil.tryRunTaosSqlAsync(sql).Wait();
        }

        public void start()
        {
            timer.Start();
        }

        public void stop()
        {
            timer.Stop();
        }

    }

    public class TaosTaskGroup
    {
        public int interval { get; set; }
        public List<TaosTask> taosTasks { get; set; }


        System.Timers.Timer _timer;
        public System.Timers.Timer timer
        {
            get
            {
                if (_timer == null)
                {
                    _timer = new System.Timers.Timer(interval);
                    _timer.Elapsed += OnTimerElapsed;
                }
                return _timer;
            }
        }

        private void OnTimerElapsed(object? sender, ElapsedEventArgs e)
        {
            var sb = new StringBuilder();
            foreach(var task in taosTasks)
            {
                foreach(var p in task.parts)
                {
                    sb.Append(p.ToString());
                }

            }
            var sql = sb.ToString();

            TaosUtil.tryRunTaosSqlAsync(sql).Wait();
        }

        public void start()
        {
            timer.Start();
        }

        public void stop()
        {
            timer.Stop();
        }


    }



    public static class Manager
    {
        static List<TaosTask> taosTasks = new List<TaosTask>();

        //static List<TaosTaskGroup> taosTaskGroups = new List<TaosTaskGroup>();
        public static void start()
        {
            foreach (var tt in taosTasks)
            {
                tt.stop();
            }

            foreach (var tc in GlobalConfig.taosTaskCfgs)
            {
                var taosTask = new TaosTask(tc);
                taosTasks.Add(taosTask);
            }

            //var gs = taosTasks.GroupBy(x => x.cfg.interval);
            //taosTaskGroups = gs.Select(x => new TaosTaskGroup
            //{
            //    interval = x.First().cfg.interval,
            //    taosTasks = x.ToList()
            //}).ToList();

            //var sb = new StringBuilder();
            foreach(var sql in GlobalConfig.initSqls)
            {
                TaosUtil.tryRunTaosSqlAsync(sql).Wait();
                //sb.AppendLine(sql);
            }
            //TaosUtil.tryRunTaosSqlAsync(sb.ToString()).Wait();


            foreach (var tt in taosTasks)
            {
                tt.start();
            }
        }
    }
}