﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taos.Data.Generator
{
    public static class TaosHistoryUtil
    {
        static bool errorFound = false;

        static void runTask(TaosHistoryTask htask)
        {
            var cfg = GlobalConfig.taosHistoryTaskCollection;

            var ts = cfg.start;
            string prefix = $"INSERT INTO {htask.table} {htask.fields} VALUES";
 

            var sb = new StringBuilder();
            int i = 0;

            var parts = TaosUtil.parseFormattedSqlString(htask.values);

            while(ts< cfg.getEnd() && !errorFound)
            {
                if(i==0)
                {
                    sb.Append(prefix);
                }

                sb.Append("('");
                sb.Append(ts.ToString("yyyy-MM-ddTHH:mm:ssK"));
                sb.Append("'");
                sb.Append(",");
                foreach(var p in parts)
                {
                    sb.Append(p.ToString());
                }

                sb.Append(")");
                i++;
                if(i== cfg.sqlsPerBatch)
                {
                    sb.Append(";");
                    var sql = sb.ToString();

                    try
                    {
                        TaosUtil.ensureSuccRunTaosSqlAsync(sql).Wait();
                        LogUtil.info($"sql:{prefix}..., done@{ts}");
                    }
                    catch(Exception ex)
                    {
                        var error = $"sql:{sql}..., {ex.Message}";
                        //throw new Exception();
                        LogUtil.error(error);
                        errorFound = true;
                        break;
                    }

                    i = 0 ;
                    sb.Clear();
                }

                var interval = htask.interval!;

                if(interval.EndsWith("s"))
                {
                    var interval_ = Convert.ToInt32(interval.TrimEnd('s'));
                    ts = ts.AddSeconds(interval_);
                }
                else if (interval.EndsWith("m"))
                {
                    var interval_ = Convert.ToInt32(interval.TrimEnd('m'));
                    ts = ts.AddMinutes(interval_);
                }
                else if (interval.EndsWith("h"))
                {
                    var interval_ = Convert.ToInt32(interval.TrimEnd('h'));
                    ts = ts.AddHours(interval_);
                }
                else if (interval.EndsWith("d"))
                {
                    var interval_ = Convert.ToInt32(interval.TrimEnd('d'));
                    ts = ts.AddDays(interval_);
                }
                else if (interval.EndsWith("w"))
                {
                    var interval_ = Convert.ToInt32(interval.TrimEnd('w'));
                    ts = ts.AddDays(interval_*7);
                }
                else if (interval.EndsWith("M"))
                {
                    var interval_ = Convert.ToInt32(interval.TrimEnd('M'));
                    ts = ts.AddMonths(interval_);
                }
                else if (interval.EndsWith("y"))
                {
                    var interval_ = Convert.ToInt32(interval.TrimEnd('y'));
                    ts = ts.AddYears(interval_);
                }
            }

            if(sb.Length > 0)
            {
                sb.Append(";");
                var sql = sb.ToString();
                TaosUtil.ensureSuccRunTaosSqlAsync(sql).Wait();
                LogUtil.info($"sql:{prefix}..., done@{ts}");
            }
        }

        public static void runTasks()
        {
            foreach (var sql in GlobalConfig.initSqls)
            {
                TaosUtil.tryRunTaosSqlAsync(sql).Wait();
            }

            var cfg = GlobalConfig.taosHistoryTaskCollection;

            var bgTasks = new List<Task>();
            foreach (var htask in cfg.tasks)
            {
                if (htask.interval == null)
                {
                    htask.interval = cfg.interval;
                }

                var bgTask = Task.Run(() => {
                    runTask(htask);
                });
                bgTasks.Add(bgTask);
            }



            Task.WaitAll(bgTasks.ToArray());

            if(errorFound)
            {
                LogUtil.error("Exit with error");
            }
            else
            {
                LogUtil.info("ALL DONE.");
            }

        }
    }
}
