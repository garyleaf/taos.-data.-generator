﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Taos.Data.Generator
{
    public class TaosUtil
    {
        static HttpClient _httpClient;
        static HttpClient httpClient
        {
            get
            {
                if (_httpClient == null)
                {
                    _httpClient = getNewHttpClient();
                }
                return _httpClient;
            }
            set
            {
                _httpClient = value;
            }
        }

        public static HttpClient getNewHttpClient()
        {
            var _httpClient = new HttpClient();
            var taosCfg = GlobalConfig.taosConfig;

            var url = $"http://{taosCfg.TAOS_HOST}:{taosCfg.TAOS_REST_PORT}/rest/sqlutc";

            var user = GlobalConfig.taosConfig.TAOS_USERNAME;
            var password = GlobalConfig.taosConfig.TAOS_PASSWORD;

            var authentication = new AuthenticationHeaderValue("Basic",
                Convert.ToBase64String(Encoding.UTF8.GetBytes($"{user}:{password}")
                ));
            _httpClient.DefaultRequestHeaders.Authorization = authentication;
            _httpClient.BaseAddress = new Uri(url);

            return _httpClient;
        }


        public static async Task<string> runTaosSqlAsync(string tsql)
        {
            var content = new StringContent(tsql);
            content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            var response = await httpClient.PostAsync("", content);

            var responseBody = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(responseBody);

                try
                {
                    response.EnsureSuccessStatusCode();
                }
                catch (Exception ex)
                {
                    throw new Exception(responseBody + Environment.NewLine + ex);
                }
            }
            return responseBody;
        }

        public static async Task<string> ensureSuccRunTaosSqlAsync(string tsql)
        {

            var responseBody = await runTaosSqlAsync(tsql);

            var resd = JsonConvert.DeserializeObject<dynamic>(responseBody);
            if(resd.status == "error")
            {
                string desc = resd.desc;

                throw new Exception(desc);
            }

            return responseBody;
        }


        public static async Task tryRunTaosSqlAsync(string tsql)
        {
            try
            {
                
                //LogUtil.trace(tsql);
                var res = await runTaosSqlAsync(tsql);
                var resd = JsonConvert.DeserializeObject<dynamic>(res);
                //if(resd.status == "succ")
                //{
                    LogUtil.info( $"{resd.status} {resd.code} {tsql} {resd.desc}");
                //}
                //else
                //{
                //    var s = JsonConvert.SerializeObject(JsonConvert.DeserializeObject<dynamic>(res), Formatting.Indented);
                //    LogUtil.info(tsql + "\n\r" + s);
                //}

            }
            catch (Exception ex)
            {
                LogUtil.error(tsql + ", exception : " +  ex.Message);
            }
        }

        static bool _taosConnOk = true;
        public static bool taosConnOk
        {
            get
            {
                return _taosConnOk;
            }
            set
            {
                _taosConnOk = value;
                if (value == false)
                {
                    LogUtil.error("taos conn lost");
                    testTaosConn();
                }
                else
                {
                    LogUtil.info("taos conn recovered.");
                }
            }
        }


        static bool testTaosConnRunning = false;
        public static void testTaosConn()
        {
            if (testTaosConnRunning)
            {
                return;
            }

            testTaosConnRunning = true;

            Task.Run(() =>
            {
                while (true)
                {
                    try
                    {
                        //httpClient = getNewHttpClient();
                        runTaosSqlAsync("show databases;").Wait();
                        taosConnOk = true;
                        testTaosConnRunning = false;
                        break;
                    }
                    catch (Exception ex)
                    {

                    }
                    Thread.Sleep(1000);
                }
            });
        }


        public static List<IString> parseFormattedSqlString(string sql)
        {
            IString parseVStr(string s)
            {
                //RB, RD, RF, RI
                try
                {
                    var txt = s.Trim().ToUpper();
                    if (s.StartsWith("RB"))
                    {
                        return new RandomBool();
                    }
                    else if (s.StartsWith("RI"))
                    {
                        int min = 0;
                        int max = 0;
                        var ss = s.Substring(2);
                        ss = ss.TrimStart('(');
                        ss = ss.TrimEnd(')');
                        var lst = ss.Split(',');
                        min = Convert.ToInt32(lst[0]);
                        max = Convert.ToInt32(lst[1]);

                        return new RandomInt(min, max);
                    }
                    else if (s.StartsWith("RF"))
                    {
                        float min = 0;
                        float max = 0;
                        var ss = s.Substring(2);
                        ss = ss.TrimStart('(');
                        ss = ss.TrimEnd(')');
                        var lst = ss.Split(',');
                        min = Convert.ToSingle(lst[0]);
                        max = Convert.ToSingle(lst[1]);

                        return new RandomFloat(min, max);
                    }
                    else if (s.StartsWith("RD"))
                    {
                        double min = 0;
                        double max = 0;
                        var ss = s.Substring(2);
                        ss = ss.TrimStart('(');
                        ss = ss.TrimEnd(')');
                        var lst = ss.Split(',');
                        min = Convert.ToDouble(lst[0]);
                        max = Convert.ToDouble(lst[1]);

                        return new RandomDouble(min, max);
                    }
                    else if (s.StartsWith("IRD"))
                    {
                        double min = 0;
                        double max = 0;
                        var ss = s.Substring(3);
                        ss = ss.TrimStart('(');
                        ss = ss.TrimEnd(')');
                        var lst = ss.Split(',');
                        min = Convert.ToDouble(lst[0]);
                        max = Convert.ToDouble(lst[1]);

                        return new IncrementalRandomDouble(min, max);
                    }
                    else if (s.StartsWith("IRI"))
                    {
                        int min = 0;
                        int max = 0;
                        var ss = s.Substring(3);
                        ss = ss.TrimStart('(');
                        ss = ss.TrimEnd(')');
                        var lst = ss.Split(',');
                        min = Convert.ToInt32(lst[0]);
                        max = Convert.ToInt32(lst[1]);

                        return new IncrementalRandomDouble(min, max);
                    }

                    throw new Exception($"unknow token type, {sql}");
                }
                catch (Exception e)
                {
                    throw new Exception($"invalid token found, {sql}, exception: {e}");
                }
            }

            var parts = new List<IString>();

            bool start = false;

            var str = "";
            var vstr = "";
            foreach (var c in sql)
            {
                if (c == '{')
                {
                    start = true;
                    if (str.Length > 0)
                    {
                        parts.Add(new RawString(str));
                    }
                    str = "";
                    continue;
                }

                if (c == '}')
                {
                    start = false;
                    IString istring = parseVStr(vstr);
                    parts.Add(istring);
                    vstr = "";
                    continue;
                }

                if (start)
                {
                    vstr += c;
                }
                else
                {
                    str += c;
                }
            }

            if (str.Length > 0)
            {
                parts.Add(new RawString(str));
            }

            return parts;
        }

    }
}
