﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taos.Data.Generator
{

    public class TaosConfig
    {
        public string TAOS_HOST = "";
        public int TAOS_REST_PORT = 6041;
        public string TAOS_USERNAME = "root";
        public string TAOS_PASSWORD = "taosdata";
    }

    public class TaosTaskConfig
    {
        public string sql;
        public int interval;
    }

    public class TaosHistoryTaskCollection
    {
        public DateTime start;
        public DateTime? end;

        public DateTime getEnd()
        {
            if (end != null) return end.Value;
            return DateTime.Now;
        }

        public int sqlsPerBatch = 1000;
        public string interval = "1s";
        public List<TaosHistoryTask> tasks;
    }

    public class TaosHistoryTask
    {
        public string table;
        public string fields = "";
        public string values;
        public string? interval;
    }


}
