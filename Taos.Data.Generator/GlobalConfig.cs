﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taos.Data.Generator
{
    public class GlobalConfig
    {

        static TaosConfig _taosConfg;
        public static TaosConfig taosConfig
        {
            get
            {
                if (_taosConfg == null)
                {
                    loadTaosConfig();
                }
                return _taosConfg;
            }
        }

        private static void loadTaosConfig()
        {
            _taosConfg = loadJsonFormatConfig<TaosConfig>("taosConfig.json");
        }

        static List<string> _initSqls;
        public static List<String> initSqls
        {
            get
            {
                if (_initSqls == null)
                {
                    loadInitSqls();
                }
                return _initSqls;
            }
        }

        private static void loadInitSqls()
        {
            _initSqls = loadJsonFormatConfig<List<String>>("taosInitSql.json");
        }

        static List<TaosTaskConfig> _taosTaskCfgs;
        public static List<TaosTaskConfig> taosTaskCfgs
        {
            get
            {
                if (_taosTaskCfgs == null)
                {
                    loadTaosTaskCfgs();
                }
                return _taosTaskCfgs;
            }
        }

        static TaosHistoryTaskCollection _taosHistoryTaskCollection;
        public static TaosHistoryTaskCollection taosHistoryTaskCollection
        {
            get
            {
                if( _taosHistoryTaskCollection == null)
                {
                    loadTaosHistoryTaskCollection();
                }
                return _taosHistoryTaskCollection;
            }
        }

        static void loadTaosHistoryTaskCollection()
        {
            _taosHistoryTaskCollection = loadJsonFormatConfig<TaosHistoryTaskCollection>("taosHistoryTask.json");
        }


        private static void loadTaosTaskCfgs()
        {
            _taosTaskCfgs = loadJsonFormatConfig<List<TaosTaskConfig>>("taosTask.json");
        }

        static string _configFolder;
        static string configFolder
        {
            get
            {
                if (_configFolder == null)
                {
                    var rootpath = System.AppDomain.CurrentDomain.BaseDirectory;
                    _configFolder = Path.Combine(rootpath, "config");
                }
                return _configFolder;
            }
        }




        static T loadJsonFormatConfig<T>(string _configFileName, bool allowNoFile = false) where T : new()
        {
            var rootPath = configFolder;

            var configFilePath = Path.Combine(rootPath, _configFileName);

            LogUtil.info($"parsing {configFilePath}...");

            if (!File.Exists(configFilePath))
            {
                if (allowNoFile)
                {
                    LogUtil.warn($"config file missing <{configFilePath}>! default setting is used.");
                    return new T();
                }
                else
                {
                    throw new Exception($"config file missing <{configFilePath}>!");
                }
            }

            var js = File.ReadAllText(configFilePath);

            var cfg = JsonConvert.DeserializeObject<T>(js);

            LogUtil.succ($"config file <{configFilePath}> parsing done.");
            return cfg;
        }


        public static void LoadConifg()
        {
            LogUtil.trace("loading config...");
            try
            {

                loadTaosConfig();
                loadInitSqls();
                loadTaosTaskCfgs();
                LogUtil.succ("load config done.");
            }
            catch (Exception ex)
            {
                LogUtil.error(ex.Message);
            }
        }



        //public static void initTaosDB()
        //{
        //    LogUtil.trace("init taos db...");
        //    try
        //    {
        //        foreach (var taosdb in taosDbConfig.taosDbs)
        //        {
        //            var sql = taosdb.getCreateSql();
        //            LogUtil.trace($"exec taossql : {sql}");
        //            TaosUtil.runTaosSqlAsync(sql).Wait();
        //        }
        //        LogUtil.info("init taos db done.");
        //    }
        //    catch (Exception ex)
        //    {
        //        LogUtil.error(ex.Message);
        //    }
        //}

        //public static void initTaosStable()
        //{
        //    LogUtil.trace("init taos stable...");
        //    try
        //    {
        //        foreach (var cfg in taosStableConfigs)
        //        {
        //            var sql = cfg.getSTableCreateSql();
        //            LogUtil.trace($"exec taossql : {sql}");
        //            TaosUtil.runTaosSqlAsync(sql).Wait();
        //        }
        //        LogUtil.info("init taos stable done.");
        //    }
        //    catch (Exception ex)
        //    {
        //        LogUtil.error(ex.Message);
        //    }
        //}
        //public static void initTaosTable()
        //{
        //    LogUtil.trace("init taos table...");
        //    try
        //    {
        //        foreach (var cfg in taosTableConfigs)
        //        {
        //            var sql = cfg.getTableCreateSql();
        //            LogUtil.trace($"exec taossql : {sql}");
        //            TaosUtil.runTaosSqlAsync(sql).Wait();
        //        }
        //        LogUtil.info("init taos table done.");
        //    }
        //    catch (Exception ex)
        //    {
        //        LogUtil.error(ex.Message);
        //    }
        //}
    }
}
