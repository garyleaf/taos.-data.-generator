﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taos.Data.Generator
{
    public class LogUtil
    {
        public static event Action<string> OnTrace;
        public static event Action<string> OnError;
        public static event Action<string> OnInfo;
        public static event Action<string> OnSucc;
        public static event Action<string> OnWarn;

        public static void info(string s)
        {
            OnInfo?.Invoke(s);
        }
        public static void succ(string s)
        {
            OnSucc?.Invoke(s);
;
        }
        public static void error(string s)
        {
            OnError?.Invoke(s);
 
        }

        public static void trace(string s)
        {
            OnTrace?.Invoke(s);
        }

        public static void warn(string s)
        {
            OnWarn?.Invoke(s);
        }
    }
}
