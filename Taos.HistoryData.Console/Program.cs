﻿global using Taos.Data.Generator;

Console.WriteLine("--------- taos history data generator ----------");

LogUtil.OnError += onError;

void onError(string obj)
{
    Console.WriteLine(DateTime.Now + " ERROR " + obj);
}

LogUtil.OnInfo += onInfo;

void onInfo(string obj)
{
    Console.WriteLine(DateTime.Now + " INFO " + obj);
}

try
{ 
    TaosHistoryUtil.runTasks();
}
catch (Exception e)
{
    LogUtil.error(e.Message);
}


while(true)
{
    var ki = Console.ReadKey();
    if(ki.Key == ConsoleKey.Escape)
    {
        break;
    }
}
