﻿// See https://aka.ms/n// See https://aka.ms/new-console-template for more information
global using Taos.Data.Generator;
using Newtonsoft.Json;

Console.WriteLine("welcome");

LogUtil.OnError += onError;

void onError(string obj)
{
    Console.WriteLine(DateTime.Now + " ERROR " + obj);
}

LogUtil.OnInfo += onInfo;

void onInfo(string obj)
{
    Console.WriteLine(DateTime.Now + " INFO " + obj);
}

LogUtil.OnSucc += onInfo;
LogUtil.OnTrace += onInfo;
LogUtil.OnWarn += onInfo;

var taosCfg = GlobalConfig.taosConfig;

var prefix = $"#{taosCfg.TAOS_HOST}:{taosCfg.TAOS_REST_PORT}:";

while (true)
{
    Console.Write(prefix);
    var s = Console.ReadLine();

    if (s == null || s.ToLower() == "exit") break;

    try
    {
        var res = await TaosUtil.runTaosSqlAsync(s);
        var json = JsonConvert.SerializeObject(JsonConvert.DeserializeObject<dynamic>(res), Formatting.Indented);
        LogUtil.info(json);
    }
    catch (Exception ex)
    {
        onError(ex.Message);
    }
}
